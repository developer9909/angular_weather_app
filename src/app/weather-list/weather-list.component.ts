import { Component, OnInit } from '@angular/core';
import { WeatherService } from '../weather.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-weather-list',
  templateUrl: './weather-list.component.html',
  styleUrls: ['./weather-list.component.css']
})
export class WeatherListComponent implements OnInit {
  data = []

  constructor(
    private weatherService: WeatherService,
    private router: Router
  ) { }

  ngOnInit() {
    this.weatherService.getWeather().subscribe((response)=>{
      this.data = response.list;
      console.log(this.data);
    })
  }

  onWeatherClicked(weather) {
    localStorage.setItem('weather',JSON.stringify(weather));
    this.router.navigateByUrl('/detail');
  }
}
